/**
 * Controller for all user details APIs
 */

import { Router, Request, Response, NextFunction } from 'express';

import { UsersService } from './../../../services/users';

const router: Router = Router();

/**
 * Get users details
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
router.get('/:emailid', async (req: Request, res: Response, next: NextFunction) => {
    try {
        res.json(await UsersService.get(req));
    } catch (err) {
        next(err);
    }
});

export default router;