/**
 * Main router which inturn routes to specific routes
 */

import { Router } from 'express';

import dummyController from './dummy';
import usersController from './users'

const router: Router = Router();

router.use('/dummy', dummyController);

router.use('/users', usersController);

export default router;