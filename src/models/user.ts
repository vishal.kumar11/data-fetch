import * as mongoose from "mongoose";
import { Schema, Document } from "mongoose";

export interface IUser extends Document{
  firstname: string;
  lastname: string;
  email: string;
  phone: string;
  rentedBooks?: Array<string>;
  address?: string
}

const UserSchema: Schema = new Schema({
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  email: { type: String, required: true },
  phone: { type: String, required: true },
  rentedBooks: [{ type: String }],
  address: { type: String },
});

export default mongoose.model<IUser>("User", UserSchema);

// import { Schema, model } from "mongoose";

// const userSchema = new Schema({
//   firstname: { type: String, required: true },
//   lastname: { type: String, required: true },
//   email: { type: String, required: true },
//   phone: { type: String, required: true },
//   rentedBooks: [{ type: String }],
//   address: { type: String },
// });

// export const User = model("User", userSchema);
