import * as mongoose from 'mongoose';
import  { Schema, Document } from 'mongoose';

export interface IAddress extends Document{
  addressId: string,
  street: string, 
  postalCode: string, 
  city: string,
  country: string,
  house: string
}

const AddressSchema = new Schema({
  addressId: { type: String, required: true },
  street: { type: String, required: true },
  postalCode: { type: String, required: true },
  city: { type: String, required: true },
  country: { type: String, required: true },
  house: { type: String, required: true }
});

export default mongoose.model<IAddress>('Address', AddressSchema);