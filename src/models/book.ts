import * as mongoose from 'mongoose';
import { Schema, Document } from "mongoose";

export interface IBook extends Document{
  bookId: string,
  isbn: string, 
  title: string, 
  subtitle: string,
  author: string,
  published: string, 
  publisher: string,
  pages: number, 
  description: string,
  website: string
}


const BookSchema = new Schema({
  bookId: { type: String, required: true },
  isbn: { type: String, required: true },
  title: { type: String, required: true },
  subtitle: { type: String, required: true },
  author: { type: String, required: true },
  published: { type: String, required: true },
  publisher: { type: String, required: true },
  pages: { type: Number, required: true },
  description: { type: String, required: true },
  website: { type: String, required: true }
});


export default mongoose.model<IBook>('Book', BookSchema);