/**
 * Service class and methods for user details APIs
 */
import User, { IUser } from "../../models/user";
import Address, { IAddress } from "../../models/address";
import Book, { IBook } from "../../models/book";

/* Type of RentedBooks */
type rentedBooksType = Array<{
  isbn: string,
  title: string,
  subtitle: string,
  author: string
  }>;

/* Type of UserDetails */
type UserDetails = {
  name: string,
  email: string,
  phone: string,
  address?: string,
  country?: string,
  rentedBooks?: rentedBooksType
};

class UsersService {
  /**
   * Method to get the user details
   * @param {Request} req
   * @returns { Promise< UserDetails | string > }
   */
  public static async get(req: any): Promise< UserDetails | string > {
    try {
      const userData: IUser = await User.findOne({
        email: req.params.emailid,
      }).lean();

      if(userData==null){
        return "User does not exit";
      }else{

        let user:UserDetails={
          name :userData.firstname + " " + userData.lastname,
          email: userData.email,
          phone: userData.phone
        }
        
        const addressData: Promise<
        { address?: string; country?: string } 
        > = UsersService.getAddressData(userData.address);
        
        const bookId: Array<string> | undefined = userData.rentedBooks;
        const bookData: Promise< rentedBooksType | string
        > = UsersService.getBookData(bookId);

        const addressBookCombined: Array<any> = await Promise.all([
          addressData,
          bookData,
        ]);

        const addressDetails = addressBookCombined[0];
        if(addressDetails){
          user.address = addressDetails.address;
          user.country = addressDetails.country;
        }

        if(addressBookCombined[1]!="no books"){
          user.rentedBooks = addressBookCombined[1];
        }

        return user;
      }
    } catch (err) {
      console.log("error occured");
      return "Try again"
    }
  }

  /**
   * Method to get the user's address details
   * @returns {Promise<{address:string,country:string}>}
   */

  public static async getAddressData(
    addressId: string | undefined
  ): Promise<{ address?: string; country?: string } > {
    try {
      console.log("finding address");
      const addressData: IAddress = await Address.findOne({
        addressId: addressId,
      }).lean();
      if(addressData==null){
        return {};
      }
      const address: string =
        addressData.house +
        "," +
        " " +
        addressData.street +
        "," +
        " " +
        addressData.city +
        " " +
        "-" +
        " " +
        addressData.postalCode;
      const country: string = addressData.country;
      return { address: address, country: country };
      
    } catch (err) {
      // console.log("error occured in getAddressData");
      // console.log(processMongoError);
      throw err;
    }
  }
  

  /**
   * Method to get the user's book details
   * @returns { Promise< rentedBooksType | string > }
   */
  public static async getBookData(
    bookId: Array<string> | undefined
  ): Promise< rentedBooksType | string >
    {
    try {
      console.log("finding book details");
     

      let bookData: Array<IBook> = await Book.find({
        bookId: { $in: bookId },
      }).lean();

      if (!bookData.length) {
        return "no books";
      }

      let rentedBooks: rentedBooksType=[];
      bookData.forEach((book) => {
        rentedBooks.push({
          isbn: book.isbn,
          title: book.title,
          subtitle: book.subtitle,
          author: book.author,
        });
      });

      return rentedBooks;
    } catch (err) {
      throw err;
    }
  }
}

export { UsersService };
